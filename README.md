![License: CC BY-NC 4.0](https://img.shields.io/badge/License-CC%20BY--NC%204.0-lightgrey.svg)

# Eye4Ref Dataset


## Overview

Eye4Ref is a rich multimodal dataset of eye-movement recordings collected from referentially complex situated settings where the linguistic utterances and their visual referential world were available to the listener. It consists of not only fixation parameters but also saccadic movement parameters that are time-locked to accompanying German utterances (with English translations). Additionally, it also contains symbolic knowledge (contextual) representations of the images to map the referring expressions onto the objects in corresponding images. Overall, the data was collected from 62 participants in three different experimental setups (86 systematically controlled sentence–image pairs and 1844 eye-movement recordings). Referential complexity was controlled by visual manipulations (e.g. number of objects, visibility of the target items, etc.), and by linguistic manipulations (e.g., the position of the disambiguating word in a sentence). This multimodal dataset, in which the three different sources of information namely eye-tracking, language, and visual environment are aligned, offers a test of various research questions not from only language perspective but also computer vision.



## Citation
- Alaçam, Özge, Eugen Ruppert, Amr R. Salama, Tobias Staron, and Wolfgang Menzel (2020). [*Eye4Ref: A Multimodal Eye Movement Dataset of Referentially Complex Situations.*](https://www.aclweb.org/anthology/2020.lrec-1.292/) In: Proceedings of the 12th International Conference (LREC 2020). Marseille, France, pp. 2396–2404. 


## License

This work is licensed under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) (cc-by-sa-4.0)
![cc-by-sa-image](https://licensebuttons.net/l/by-sa/4.0/88x31.png)
