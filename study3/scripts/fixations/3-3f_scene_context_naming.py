from multiprocessing import Pool, cpu_count
import pandas as pd
import os, time



in_path="fixationbased/2-fixationbased_by_scene_ling/"
out_path="fixationbased/3-fixationbased_by_scene_ling_context/"

context_filename = "orig_data/study1_contextual_relations.xlsx"
overlapping_filename = "orig_data/study1_overlapping_AOI.xlsx"



def context_naming(file):
  current =os.path.join(in_path, file)
  print("Processing file " + file)
  
  # input
  df=pd.read_csv(current, encoding = 'utf8')
  
  participant = df['RECORDING_SESSION_LABEL'].values[0]
  scene = df['ItemNO'].values[0]
  
  #if scene != "A6":
    #return
  
  
  # input xlsx sheet with context description
  contextfile=pd.ExcelFile(context_filename, encoding = 'utf8')
  dfc=pd.read_excel(contextfile,scene)
  
  # input xlsx sheet with overlapping aoi objects
  overlapfile=pd.ExcelFile(overlapping_filename, encoding = 'utf8')
  dfo=pd.read_excel(overlapfile,scene)
  
  df["CURRENT_FIX_INTEREST_AREAS_Labelled"]=""
  df["PREVIOUS_SAC_START_INTEREST_AREAS_Labelled"]=""
  df["NEXT_SAC_END_INTEREST_AREAS_Labelled"]=""
  
  # get list AOIs, remove 99 
  # add linguistics label
  for index, row in df.iterrows():
    
    # add colums for area of interest fixations
    curr_aois = cleanup_AOI_list(row["CURRENT_FIX_INTEREST_AREAS"])
    prev_aois = cleanup_AOI_list(row["PREVIOUS_SAC_START_INTEREST_AREAS"])
    next_aois = cleanup_AOI_list(row["NEXT_SAC_END_INTEREST_AREAS"])
    
    curr_aois_labelled = add_ling_info(curr_aois, dfc)
    df.at[index,"CURRENT_FIX_INTEREST_AREAS_Labelled"] = curr_aois_labelled
    df.at[index,"CURRENT_FIX_INTEREST_AREAS"] = curr_aois
    
    #prev_aois_labelled = add_ling_info(prev_aois, dfc)
    #df.at[index,"PREVIOUS_SAC_START_INTEREST_AREAS_Labelled"] = prev_aois_labelled 
    #df.at[index,"PREVIOUS_SAC_START_INTEREST_AREAS"] = prev_aois
    
    #next_aois_labelled = add_ling_info(next_aois, dfc)
    #df.at[index,"NEXT_SAC_END_INTEREST_AREAS_Labelled"] = next_aois_labelled
    #df.at[index,"NEXT_SAC_END_INTEREST_AREAS"] = next_aois

#SR_SOI_No	SR_AOI_Label	Context_label

  print(file)
    
#add normalization parameters (pupil -- per participant over all scenes, internal fixation duration, Norm_PS_Duration,Norm_PS_Amplitude	Norm_PS_AvgVelocity	Norm_PS_PeakVelocity,Norm_NS_Duration	Norm_NS_Amplitude	Norm_NS_AvgVelocity	Norm_NS_PeakVelocity)

  max_fix_dur  = df['CURRENT_FIX_DURATION'].max()
  df["NORMALIZED_CURRENT_FIX_DURATION"] = df['CURRENT_FIX_DURATION'] / max_fix_dur
    
  max_pupil_size  = max(abs(df['CURRENT_FIX_PUPIL'].max()), abs(df['CURRENT_FIX_PUPIL'].min()))
  df["NORMALIZED_CURRENT_FIX_PUPIL"] = df['CURRENT_FIX_PUPIL'] / max_pupil_size
  
  max_prev_sac_dur  = df['PREVIOUS_SAC_DURATION'].max()
  df["NORMALIZED_PREVIOUS_SAC_DURATION"] = df['PREVIOUS_SAC_DURATION'] / max_prev_sac_dur
  
  max_prev_sac_amp  = df['PREVIOUS_SAC_AMPLITUDE'].max()
  df["NORMALIZED_PREVIOUS_SAC_AMPLITUDE"] = df['PREVIOUS_SAC_AMPLITUDE'] / max_prev_sac_amp
  
  max_prev_avg_vel  = df['PREVIOUS_SAC_AVG_VELOCITY'].max()
  df["NORMALIZED_PREVIOUS_SAC_AVG_VELOCITY"] = df['PREVIOUS_SAC_AVG_VELOCITY'] / max_prev_avg_vel
  
  max_prev_sac_peak_vel  = df['PREVIOUS_SAC_PEAK_VELOCITY'].max()
  df["NORMALIZED_PREVIOUS_SAC_PEAK_VELOCITY"] = df['PREVIOUS_SAC_PEAK_VELOCITY'] / max_prev_sac_peak_vel
  

  print(pd.to_numeric(df['NEXT_SAC_DURATION'], errors='coerce'))
  max_next_sac_dur  = pd.to_numeric(df['NEXT_SAC_DURATION'], errors='coerce').max()
  df["NORMALIZED_NEXT_SAC_DURATION"] = df['NEXT_SAC_DURATION'] / max_next_sac_dur
  
  max_next_sac_amp  = df['NEXT_SAC_AMPLITUDE'].max()
  df["NORMALIZED_NEXT_SAC_AMPLITUDE"] = df['NEXT_SAC_AMPLITUDE'] / max_next_sac_amp
  
  max_next_avg_vel  = df['NEXT_SAC_AVG_VELOCITY'].max()
  df["NORMALIZED_NEXT_SAC_AVG_VELOCITY"] = df['NEXT_SAC_AVG_VELOCITY'] / max_next_avg_vel
  
  max_next_sac_peak_vel  = df['NEXT_SAC_PEAK_VELOCITY'].max()
  df["NORMALIZED_NEXT_SAC_PEAK_VELOCITY"] = df['NEXT_SAC_PEAK_VELOCITY'] / max_next_sac_peak_vel

        
  ## save files
  if not os.path.exists(out_path):
    os.makedirs(out_path)
  
  df.to_csv(out_path+"study1_timeseries_20ms_bins_ling_context_"+participant+"_"+scene+".csv", index=False, encoding = 'utf8')

def cleanup_AOI_list(list_string):
  items_aoi=[]
  items = list_string.replace("[", "").replace("]", "").replace(".","").strip().split(",")
  if len(items)>0 and items[0]:
    items_aoi = map(int or 99, items)
  if 99 in items_aoi:
    items_aoi.remove(99)
  return items_aoi     
  
  
  
def add_ling_info(list, dfc):
  labels=[]
  for aoi in list:
    labeldf = dfc.loc[dfc["SR_SOI_No"]==aoi,["Context_label"]]
    # if we have a label for AOI in the context file
    if len(labeldf["Context_label"]>0):
      label= labeldf["Context_label"].values[0]
      labels.append(label)
  return labels      
        

if __name__ == '__main__':
  start = time.time()
  
  # process files in parallel
  file_list = list(os.listdir(in_path))
  nr_processes = min(len(file_list), cpu_count())
  
  print("Processing {0} files using {1} parallel processes.".format(len(file_list),nr_processes))
  p = Pool(nr_processes)
  #p.map(context_naming, file_list)
  
  for file in file_list:
    context_naming(file)
  
  end = time.time()
  print("Processing took {0:4.1f} seconds.".format((end - start)))
