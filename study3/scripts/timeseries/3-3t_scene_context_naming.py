from multiprocessing import Pool, cpu_count
import pandas as pd
import os, time

in_path="timeseries/2-timeseries-binned_ling/"
out_path="timeseries/3-timeseries-binned_ling_context/"
ling_annotations="orig_data/study3_timings_ling_labels_nogap.xlsx"

context_filename = "orig_data/study3_contextual_relations.xlsx"
overlapping_filename = "orig_data/study3_overlapping_AOI.xlsx"

# fixation threshold when the fixation is counted as a target match (out of 20, by default)
fix_threshold = 10

def decrease_parents(dataframe, index, value, overlaps, key):
  """ This function splits the fixations on overlappping area-of-interests. Example; there are two objects on the scene; a mug and a table. and the mug is located on the table. In that case, any fixationon the mugh will be also counted for the table in case the AOI for table covers all the objects located on it. In that case, if the fixation is specifically on the mug, then the fixation on the table is removed. """
  #print (key)
  #if key == "NA":
  #  return
  dataframe.at[index,"AVERAGE_DESEGMENTED_IA_"+key+"_SAMPLE_COUNT"] = max(dataframe.at[index,"AVERAGE_DESEGMENTED_IA_"+key+"_SAMPLE_COUNT"] - value ,0)
  dataframe.at[index,"AVERAGE_DESEGMENTED_IA_"+key+"_SAMPLE_COUNT_%"] = max(dataframe.at[index,"AVERAGE_DESEGMENTED_IA_"+key+"_SAMPLE_COUNT_%"] - value ,0)
  #print("new value ", df.at[index,"AVERAGE_DESEGMENTED_IA_"+key+"_SAMPLE_COUNT"])
  if overlaps[key] != 'NA' and dataframe.at[index, "AVERAGE_DESEGMENTED_IA_"+overlaps[key]+"_SAMPLE_COUNT"] > 0:
      decrease_parents(dataframe, index, value, overlaps, overlaps[key])


def context_naming(file):
  current =os.path.join(in_path, file)
  print("Processing file " + file)
  
  # input
  df=pd.read_csv(current, encoding = 'utf8')
  
  participant = df['RECORDING_SESSION_LABEL'].values[0]
  scene = df['ItemNO'].values[0]


  if scene.endswith("B3_2"):
    sep = scene.index('_2')
    scene = scene[0:sep]
    
  short_scene= scene[0:scene.index("_")]
  #print(scene, short_scene)
  
  #toskip = ["S20", "S21","S17", "S32", "S3"]
  #to skip incomplete scene files
  if (scene == "features"):
      return
  
  df["MOST_FIXATED_OBJECT"] = None

  # input xlsx sheet with context description
  contextfile=pd.ExcelFile(context_filename, encoding = 'utf8')
  dfc=pd.read_excel(contextfile,scene)
  
  # input xlsx sheet with overlapping aoi objects
  overlapfile=pd.ExcelFile(overlapping_filename, encoding = 'utf8')
  dfo=pd.read_excel(overlapfile,scene)
  
  lingfile=pd.ExcelFile(ling_annotations, encoding = 'utf8')
  dfl=pd.read_excel(lingfile,short_scene)
  
  # for each AOI
  # check whether exists -- this indicates AOI with several parts, these need to be combined
  # if exists: copy column, rename
  context_labels = []
  for aoi in range(1,41):
    #print aoi
    labeldf = dfc.loc[dfc["SR_SOI_No"]==aoi,["Context_label"]]
    
    # if we have a label for AOI in the context file
    if len(labeldf["Context_label"]>0):
      label= labeldf["Context_label"].values[0]
      context_labels.append(label)
      # create new labelled column in the scene with copied values from AOI
      # also create desegmented columns
      if "AVERAGE_IA_"+label+"_SAMPLE_COUNT" in df.columns:
        print("duplicate label: "+label)
        df["AVERAGE_IA_"+label+"_SAMPLE_COUNT"] =df[["AVERAGE_IA_"+str(aoi)+"_SAMPLE_COUNT","AVERAGE_IA_"+label+"_SAMPLE_COUNT"]].agg("sum",axis=1)
        df["AVERAGE_IA_"+label+"_SAMPLE_COUNT_%"] =df[["AVERAGE_IA_"+str(aoi)+"_SAMPLE_COUNT_%","AVERAGE_IA_"+label+"_SAMPLE_COUNT_%"]].agg("sum",axis=1)
        # desegmented copy (preparation)
        df["AVERAGE_DESEGMENTED_IA_"+label+"_SAMPLE_COUNT"] = df["AVERAGE_IA_"+label+"_SAMPLE_COUNT"]
        df["AVERAGE_DESEGMENTED_IA_"+label+"_SAMPLE_COUNT_%"] = df["AVERAGE_IA_"+label+"_SAMPLE_COUNT_%"]
      else:
        df["AVERAGE_IA_"+label+"_SAMPLE_COUNT"] = df["AVERAGE_IA_"+str(aoi)+"_SAMPLE_COUNT"]
        df["AVERAGE_IA_"+label+"_SAMPLE_COUNT_%"] = df["AVERAGE_IA_"+str(aoi)+"_SAMPLE_COUNT_%"]
        # desegmented copy (preparation)
        df["AVERAGE_DESEGMENTED_IA_"+label+"_SAMPLE_COUNT"] = df["AVERAGE_IA_"+str(aoi)+"_SAMPLE_COUNT"]
        df["AVERAGE_DESEGMENTED_IA_"+label+"_SAMPLE_COUNT_%"] = df["AVERAGE_IA_"+str(aoi)+"_SAMPLE_COUNT_%"]
      
  
  # get overlap dictionary {"item", "containing_item"}
  dfo.fillna("NA", inplace = True)
  overlaps = dict(zip(dfo.Context_label, dfo['part-of']))
  

  # check overlapping desegmentation, adjust values
  for index, row in df.iterrows():
    for key in overlaps.keys():
      # if we have an overlap between key and its containing_item (!=NA)
      if key != "NA" and row["AVERAGE_DESEGMENTED_IA_"+key+"_SAMPLE_COUNT"] > 0 and overlaps[key] != "NA":
        value = row["AVERAGE_DESEGMENTED_IA_"+key+"_SAMPLE_COUNT"]
        # decrease values of all parents recursively
        decrease_parents(df, index, value, overlaps, overlaps[key])
    
    ## get most fixated object    
    object_fixations = {}
    for label in context_labels:
      object_fixations[label] = row["AVERAGE_DESEGMENTED_IA_"+label+"_SAMPLE_COUNT"]
      
    fix_sum = sum(object_fixations.values())
    if fix_sum >0:
      max_label = max(object_fixations, key=object_fixations.get)
      df.at[index, "MOST_FIXATED_OBJECT"] = max_label
        
        
  # get normalization for eye parameters AVERAGE_ACCELERATION (X/Y), AVERAGE_PUPIL_SIZE, AVERAGE_VELOCITY (X/Y)
  max_accel_x  = max(abs(df['AVERAGE_ACCELERATION_X'].max()), abs(df['AVERAGE_ACCELERATION_X'].min()))
  max_accel_y  = max(abs(df['AVERAGE_ACCELERATION_Y'].max()), abs(df['AVERAGE_ACCELERATION_Y'].min()))
  df["NORMALIZED_AVERAGE_ACCELERATION_X"] = df['AVERAGE_ACCELERATION_X'] / max_accel_x
  df["NORMALIZED_AVERAGE_ACCELERATION_Y"] = df['AVERAGE_ACCELERATION_Y'] / max_accel_y
  
  max_pupil_size  = df['AVERAGE_PUPIL_SIZE'].max()
  df["NORMALIZED_AVERAGE_PUPIL_SIZE"] = df['AVERAGE_PUPIL_SIZE'] / max_pupil_size
  
  max_vel_x  = max(abs(df['AVERAGE_VELOCITY_X'].max()), abs(df['AVERAGE_VELOCITY_X'].min()))
  max_vel_y  = max(abs(df['AVERAGE_VELOCITY_Y'].max()), abs(df['AVERAGE_VELOCITY_Y'].min()))
  df["NORMALIZED_AVERAGE_VELOCITY_X"] = df['AVERAGE_VELOCITY_X'] / max_vel_x
  df["NORMALIZED_AVERAGE_VELOCITY_Y"] = df['AVERAGE_VELOCITY_Y'] / max_vel_y

  df = add_targetspecific_column(df, dfl)      
  
  
  
  ## save files
  if not os.path.exists(out_path):
    os.makedirs(out_path)
  
  df.to_csv(out_path+"study3_timeseries_20ms_bins_ling_context_"+participant+"_"+scene+".csv", index=False, encoding = 'utf8')

def add_targetspecific_column(df, dfl):
  # find target and other communicational objects
  target_obj = dfl.loc[(dfl["TargetObj"]==1), ['ObjectLabel']].values[0][0]
  othercomm_obj = flatten(dfl.loc[(dfl["TargetLoc"]==1), ['ObjectLabel']].values.tolist())
  
  #target_loc = dfl.loc[(dfl["TargetLoc"]==1), ['ObjectLabel']].values[0][0]
  df["AVERAGE_IA_TargetObj_SAMPLE_COUNT"] = df["AVERAGE_IA_"+target_obj+"_SAMPLE_COUNT"]
  df["AVERAGE_IA_TargetObj_SAMPLE_COUNT_%"] = df["AVERAGE_IA_"+target_obj+"_SAMPLE_COUNT_%"]
  #df["AVERAGE_IA_TargetLoc_SAMPLE_COUNT"] = df["AVERAGE_IA_"+target_loc+"_SAMPLE_COUNT"]
  #df["AVERAGE_IA_TargetLoc_SAMPLE_COUNT_%"] = df["AVERAGE_IA_"+target_loc+"_SAMPLE_COUNT_%"]
  
  
  othercomm_obj_countlabels=[]
  othercomm_obj_percentagelabels=[]
  
  for item in othercomm_obj:
    othercomm_obj_countlabels.append("AVERAGE_IA_"+item+"_SAMPLE_COUNT")
    othercomm_obj_percentagelabels.append("AVERAGE_IA_"+item+"_SAMPLE_COUNT_%")
  
  df["AVERAGE_IA_OtherCommObj_SAMPLE_COUNT"] =df[othercomm_obj_countlabels].agg("sum",axis=1)
  df["AVERAGE_IA__OtherCommObj__SAMPLE_COUNT_%"] =df[othercomm_obj_percentagelabels].agg("sum",axis=1)
  
  # create classification labels for target and comm objects views
  df["Label_Target"] = 0
  df.loc[df['AVERAGE_IA_TargetObj_SAMPLE_COUNT'] > fix_threshold, 'Label_Target'] = 1
  
  df["Label_OtherCommObj"] = 0
  df.loc[df['AVERAGE_IA_OtherCommObj_SAMPLE_COUNT'] > fix_threshold, 'Label_OtherCommObj'] = 1
  
  
  df["Label_All_Relevant_Items"] = df[['Label_Target', 'Label_OtherCommObj']].agg("max",axis=1)
        
  return df
  
def flatten(list):
  flat =[]
  for l in list:
    for i in l:
      flat.append(str(i))
  return flat
  
  
if __name__ == '__main__':
  start = time.time()
  
  # process files in parallel
  file_list = list(os.listdir(in_path))
  nr_processes = min(len(file_list), cpu_count())
 
  for file in file_list[0:200]:
    context_naming(file)
    #break
  
  #print("Processing {0} files using {1} parallel processes.".format(len(file_list),nr_processes))
  #p = Pool(nr_processes)
  #p.map(context_naming, file_list)
  
  end = time.time()
  print("Processing took {0:4.1f} seconds.".format((end - start)))
