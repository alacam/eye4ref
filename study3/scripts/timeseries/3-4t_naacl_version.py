from multiprocessing import Pool, cpu_count
import pandas as pd
import os, time

in_path="timeseries/3-timeseries-binned_ling_context/"
out_path="timeseries/4-timeseries-binned_ling_context_naacl/"

# read the context files
metadata_filename = "orig_data/context/contextual_relations_metadata.xlsx"
property_filename = "orig_data/context/contextual_relations_properties.xlsx"


# input xlsx sheet with context description
metafile=pd.ExcelFile(metadata_filename, encoding = 'utf8')

  
# input xlsx sheet with overlapping aoi objects
propertyfile=pd.ExcelFile(property_filename, encoding = 'utf8')
  

def add_complexity_param(file, header_written):
  current =os.path.join(in_path, file)
  print("Processing file " + file)
  
  # input
  df=pd.read_csv(current, encoding = 'utf8')
  
  scene = df['ItemNO'].values[0]
  
  
  if scene.endswith("B3_2"):
    sep = scene.index('_2')
    scene = scene[0:sep]
    
  short_scene= scene[0:scene.index("_")]
  
  
  
  #toskip = ["S20", "S21","S17", "S32", "S3"]
  #to skip incomplete scene files
  if (scene == "features"):
      return
  
  #if scene != "A6":
    #return
  dfm=pd.read_excel(metafile,scene)
  dfp=pd.read_excel(propertyfile,scene)

  allowed_columns =['AVERAGE_ACCELERATION_X', 
'AVERAGE_ACCELERATION_Y', 
'AVERAGE_GAZE_X', 
'AVERAGE_GAZE_Y',
'AVERAGE_INTEREST_AREAS',
'AVERAGE_IN_BLINK',
'AVERAGE_IN_SACCADE',
'AVERAGE_PUPIL_SIZE',
'AVERAGE_VELOCITY_X',
'AVERAGE_VELOCITY_Y',
'BIN_INDEX',
'ItemNO',
'RECORDING_SESSION_LABEL',
'RESOLUTION_X',
'RESOLUTION_Y',
'TRIAL_LABEL',
'viewtype',
'word_index',
'Label',
'word_onset',
'word_offset',
'word_duration',
'wordDE',
'wordEN',
'REFWord',
'TargetObj',
'OtherCommObj',
'ObjectLabel',
'NORMALIZED_AVERAGE_ACCELERATION_X',
'NORMALIZED_AVERAGE_ACCELERATION_Y',
'NORMALIZED_AVERAGE_PUPIL_SIZE',
'NORMALIZED_AVERAGE_VELOCITY_X',
'NORMALIZED_AVERAGE_VELOCITY_Y',
'AVERAGE_IA_TargetObj_SAMPLE_COUNT',
'AVERAGE_IA_TargetObj_SAMPLE_COUNT_%',
'AVERAGE_IA_OtherCommObj_SAMPLE_COUNT',
'AVERAGE_IA__OtherCommObj__SAMPLE_COUNT_%',
'MOST_FIXATED_OBJECT']

  df = df[allowed_columns]


  context_similarity_columns=['targetobj_similarity_count',
	'targetobj_similarity_precision',
	'targetobj_similarity_recall',
	'targetloc_similarity_count',
	'targetloc_similarity_precision',
	'targetloc_similarity_recall',
	'$colorP$',
	'$colorP$_shared_count',
	'$colorP$_shared_percentage',
	'$colorP$_match_targetobj',
	'$colorP$_match_targetloc',
	'$Location$',
	'$Location$_shared_count',
	'$Location$_shared_percentage',
	'$Location$_match_targetobj',
	'$Location$_match_targetloc',
	'$NextTo$',
	'$NextTo$_shared_count',
	'$NextTo$_shared_percentage',
	'$NextTo$_match_targetobj',
	'$NextTo$_match_targetloc',
	'$type$',
	'$type$_shared_count',
	'$type$_shared_percentage',
	'$type$_match_targetobj',
	'$type$_match_targetloc']
	
	
  for col in context_similarity_columns:
    df[col] = None
	
  for index, row in df.iterrows():
  
    mostfixated_obj= row['MOST_FIXATED_OBJECT']
    
    similarity_param = dfm.loc[(dfm["Context_label"]==mostfixated_obj),context_similarity_columns]
    

    if not isinstance(mostfixated_obj, float):
      for col in context_similarity_columns:
        row[col]= similarity_param[col].values[0]
    
    df.loc[index]=row
  
  ## save files
  if not os.path.exists(out_path):
    os.makedirs(out_path)
  
  if header_written:
    df.to_csv(out_path+"study3_timeseries_20ms_bins_ling_context_"+"par"+"_"+"scene_naacl.csv", index=False, encoding = 'utf8', mode='a',  header=False)
  else:
    df.to_csv(out_path+"study3_timeseries_20ms_bins_ling_context_"+"par"+"_"+"scene_naacl.csv", index=False, encoding = 'utf8')
    header_written = True
  return header_written






def flatten(list):
  flat =[]
  for l in list:
    for i in l:
      flat.append(str(i))
  return flat
  
  
if __name__ == '__main__':
  start = time.time()
  
  header_written = False
  
  # process files in parallel
  file_list = list(os.listdir(in_path))
  nr_processes =1
  
  print("Processing {0} files sequentially.".format(len(file_list)))
  for file in file_list:
    header_written = add_complexity_param(file, header_written)

  
  end = time.time()
  print("Processing took {0:4.1f} seconds.".format((end - start)))
