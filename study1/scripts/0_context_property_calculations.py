from multiprocessing import Pool, cpu_count
import pandas as pd
import numpy as np
import os, time
from pydoc import help
from openpyxl import load_workbook

in_path="orig_data/"
out_path="orig_data/context"
contextual_relations="orig_data/study1_contextual_relations.xlsx"
ling_annotations="orig_data/study1_timings_ling_annotations_amb.xlsx"



def process_relationfile():
  """ 
  This script first splits the contextual file into AOI info and properties. Then it computetes referential complexity and similarity
  """
  df=pd.read_excel(contextual_relations, sheet_name = None, encoding = 'utf8')
  # create output folder
  if not os.path.exists(out_path):
    os.makedirs(out_path)
  
  propertywriter = pd.ExcelWriter(out_path + "/contextual_relations_properties.xlsx", engine = 'openpyxl', encoding = 'utf8')
  metadatawriter = pd.ExcelWriter(out_path + "/contextual_relations_metadata.xlsx", engine = 'openpyxl', encoding = 'utf8')
  
  #to skip incomplete scene files
  toskip=["A9", "A10", "A11", "A12", "A15", "A16", "A17", "A18", "A19", "A21", "A22", "A23", "A24"]
  
  for scene in df.keys():
    if (scene == "features" or scene=="relations" or scene in toskip):
      continue
    
    print("Processing scene: "+scene)
    df_scene=pd.read_excel(contextual_relations, sheet_name = scene, encoding = 'utf8')
    
    #sep= scene.index("_")
    #scene_short= scene[0:sep] 
    dfl_scene=pd.read_excel(ling_annotations, sheet_name = scene, encoding = 'utf8')
    
    # store properties
    df_property = df_scene[['Predicate', 'Relation', 'Argument']]
    df_property = refcomplexity_properties(df_property)
    df_property.to_excel(propertywriter, sheet_name = scene, index=None)
 
 
    # store context metadata
    df_metadata = df_scene[['StudyNo', 'ImageNo', 'ObjectNo', 'Entity_ID',	
      'SR_AOI_comb_name'	,'SR_SOI_No',	'SR_AOI_Label',	'Context_label']]#,	'label_2',	'label_3']]
    df_metadata = refcomplexity_metadata(df_metadata, df_property, dfl_scene)
    df_metadata.to_excel(metadatawriter, sheet_name = scene, index=None)
    
  metadatawriter.save()
  metadatawriter.close()
  propertywriter.save() 
  propertywriter.close()     
    



def refcomplexity_properties(df):
  df['UniqueValues']=0
  df['#Values']=0
  df['Distinctiveness']=0
  
  # get list of properties
  df.dropna(
    axis=0,
    how='any',
    thresh=None,
    subset=None,
    inplace=True)
    
  relations = df['Relation'].unique().tolist()
  
  #for each property, get number and distinctiveness of values
  for relation in relations:
    count_all=len(df.loc[(df["Relation"]==relation), ['Argument']]['Argument'].tolist())
    #print(relation)
    count_unique=len(df.loc[(df["Relation"]==relation), ['Argument']]['Argument'].unique().tolist())
    df.loc[(df["Relation"]==relation), ['UniqueValues']]=count_unique
    df.loc[(df["Relation"]==relation), ['#Values']]=count_all
    df.loc[(df["Relation"]==relation), ['Distinctiveness']]=(count_unique*1.0/count_all)
  return df




def refcomplexity_metadata(df,df_property,dfl):
  
  df.dropna(subset = ["Context_label"], inplace=True)
  
  df["targetobj_similarity_count"]=0
  df["targetobj_similarity_precision"]=0.0
  df["targetobj_similarity_recall"]=0.0 
  
  df["targetloc_similarity_count"]=0
  df["targetloc_similarity_precision"]=0.0
  df["targetloc_similarity_recall"]=0.0 
  
  # find target
  df_target = dfl[['TargetObj',	'ObjectLabel']]
  target_obj = dfl.loc[(dfl["TargetObj"]==1), ['ObjectLabel']].values[0][0]
  #target_loc = dfl.loc[(dfl["TargetLoc"]==1), ['ObjectLabel']].values[0][0]
  
  relations = df_property['Relation'].unique().tolist()
  
  targetobj_relations={}
  #targetloc_relations={}
  
  
  # get target properties
  for relation in relations:
    targetobj_arguments= df_property.loc[(df_property["Predicate"]==target_obj) & (df_property["Relation"]==relation), ['Argument']].values.tolist()
    targetobj_relations[relation]=flatten(targetobj_arguments)
    
    #targetloc_arguments= df_property.loc[(df_property["Predicate"]==target_loc) & (df_property["Relation"]==relation), ['Argument']].values.tolist()
    #targetloc_relations[relation]=flatten(targetloc_arguments)
    
  
   # create columns for the check the similarity metrics to the target obj and loc
  for relation in relations: 
    df[relation]=None
    # add columns for the shared number of properties among all objects
    df[relation+"_shared_count"]=0
    df[relation+"_shared_percentage"]=0.0
    df[relation+"_match_targetobj"]=False
    #df[relation+"_match_targetloc"]=False
  
  # add relations 
  for index, row in df.iterrows():
    object = row['Context_label']
    df_obj_relations = df_property.loc[(df_property["Predicate"]==object), ['Relation', 'Argument']]
    obj_relations = df_obj_relations['Relation'].unique().tolist()
    
    # skip objects without relations
    if len(obj_relations) == 0:
      continue
    
    obj_relations_arguments = {}
    
    for obj_relation in obj_relations: 
      # get relation arguments
      arguments = flatten(df_obj_relations.loc[(df_obj_relations["Relation"]==obj_relation), ['Argument']].values.tolist())
      df.at[index, obj_relation]= arguments
      obj_relations_arguments[obj_relation] = arguments
      
      # relation argument count and percentage
      shared_arguments = df_property.loc[(df_property['Argument'].isin(arguments)) & (df_property['Relation']== obj_relation) & ~(df_property['Predicate']== object), ['Predicate','#Values']]
      shared_argument_count = shared_arguments['Predicate'].size
      
      df.at[index, obj_relation+"_shared_count"]= shared_argument_count
      if shared_arguments['#Values'].size > 0:
        df.at[index, obj_relation+"_shared_percentage"]= shared_argument_count *1.0 / shared_arguments['#Values'].values[0]

      # compare relation arguments to target and target location
      df.at[index, obj_relation+"_match_targetobj"]= compare_arguments(arguments, targetobj_relations[obj_relation])
      #df.at[index, obj_relation+"_match_targetloc"]= compare_arguments(arguments, targetloc_relations[obj_relation])
      
    # calculate similarity to targetobj and loc
    targetobj_overlap= compute_overlap(targetobj_relations, obj_relations_arguments)
    #targetloc_overlap= compute_overlap(targetloc_relations, obj_relations_arguments)
 
    # add similarity counts
    df.at[index, "targetobj_similarity_count"]= targetobj_overlap
    #df.at[index, "targetloc_similarity_count"] = targetloc_overlap
    obj_precision, obj_recall = compute_precision_recall(count_arguments(targetobj_relations),count_arguments(obj_relations_arguments),targetobj_overlap)
    
    
    #loc_precision, loc_recall = compute_precision_recall(count_arguments(targetloc_relations),count_arguments(obj_relations_arguments),targetloc_overlap)
    df.at[index, "targetobj_similarity_precision"]=  obj_precision 
    df.at[index, "targetobj_similarity_recall"]= obj_recall
    #df.at[index, "targetloc_similarity_precision"]=  loc_precision 
   # df.at[index, "targetloc_similarity_recall"]= loc_recall
    
  return df  

def compute_precision_recall(target_relation_count, object_relation_count, shared_count):
  precision= shared_count*1.0 / target_relation_count
  recall = shared_count*1.0 / object_relation_count
  return precision, recall

def compute_overlap(dict1, dict2):
  count=0
  for relation in dict1.keys():
    l1=dict1[relation]
    if relation in dict2.keys():
      l2=dict2[relation]
      for item in l1:
        if item in l2:
          count+=1
  return count    
      
def compare_arguments(list1, list2):
  for arg_item in list1:
    if arg_item in list2:
      return True
  return False    
  
def count_arguments(dict):
  count=0
  for key, value in dict.items():
    count+=len(value)
  return count

def flatten(list):
  flat =[]
  for l in list:
    for i in l:
      flat.append(str(i))
  return flat

if __name__ == '__main__':
  start = time.time()
  process_relationfile()
  end= time.time()
  print("Processing took {0:4.1f} minutes.".format((end - start)/60))

