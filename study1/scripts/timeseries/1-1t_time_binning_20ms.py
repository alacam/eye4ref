from multiprocessing import Pool, cpu_count
import pandas as pd
import os, time
from pydoc import help

in_path="orig_data/timeseries_reports/"
out_path="timeseries/1-timeseries-binned/"

bin_size = 20

def bin_file(file):
  """ 
  This script processes sample report data (for each participant & with 1000 sampling frequency) exported from SR Eyelink DataViewer, 
  maps AOI IDs with meaningful contextual labels, and performs 20msec-binning procedure. Finally, it splits the data by scene. 
  """
  
  current = os.path.join(in_path, file)
  print("Processing " + file)
  
  ##### this code will work until these files are manually inspected!
  if (file.endswith("r.csv") or file.endswith("15.csv") or file.endswith("13.csv")):
    print("Skipping file " + file)
    return
  
  df=pd.read_csv(current, encoding = 'utf8', low_memory=False)

  ## remove unneccessary columns
  df.drop(inplace=True, columns=["AVERAGE_INTEREST_AREA_DATA"])
  df.drop(inplace=True, columns=["SAMPLE_INDEX"])
  df.drop(inplace=True, columns=["GROUPING_VARIABLES"])

  # get participant id
  participant = df.RECORDING_SESSION_LABEL.values[0]
  
  # grouping input df by scene
  scene_groups=df.groupby(df.ItemNO)

  for scene_name,df_scene in scene_groups:
    print("Processing scene: "+scene_name)
    # remove filler trials
    if (scene_name == "." or scene_name == "UNDEFINED"):
      print("... skipping filler trial")
      continue
    
    # initialize output scene df
    df_scene_out = pd.DataFrame(columns=df.columns)
  
    
    # get starttime and end time for scene trial
    start_time = df_scene['TRIAL_START_TIME'].values[0]
    bin_start = start_time
    bin_index = 1
    
    slice = df_scene[(df_scene["TIMESTAMP"] >= bin_start)]
    
   
    # get slice of max 20 elements (bin_size)
    for i, bin_slice in  slice.groupby(slice.index // bin_size):
      if len(bin_slice) == bin_size:
        bin = bin_slice.head(1).copy()
        
        bin["BIN_INDEX"] = bin_index
        bin["BIN_START_TIME"] = bin_start
        bin["BIN_END_TIME"] = bin_start+bin_size-1
        
        # average over bin
        bin["AVERAGE_IN_BLINK"] = pd.to_numeric(bin_slice["AVERAGE_IN_BLINK"], errors='coerce').mean()
        bin["AVERAGE_IN_SACCADE"] = pd.to_numeric(bin_slice["AVERAGE_IN_SACCADE"], errors='coerce').mean()
        bin["AVERAGE_GAZE_X"] = pd.to_numeric(bin_slice["AVERAGE_GAZE_X"], errors='coerce').mean()
        bin["AVERAGE_GAZE_Y"] = pd.to_numeric(bin_slice["AVERAGE_GAZE_Y"], errors='coerce').mean()
        bin["AVERAGE_ACCELERATION_X"] = pd.to_numeric(bin_slice["AVERAGE_ACCELERATION_X"], errors='coerce').mean()
        bin["AVERAGE_ACCELERATION_Y"] = pd.to_numeric(bin_slice["AVERAGE_ACCELERATION_Y"], errors='coerce').mean()
        bin["AVERAGE_PUPIL_SIZE"] = pd.to_numeric(bin_slice["AVERAGE_PUPIL_SIZE"], errors='coerce').mean()
        bin["AVERAGE_VELOCITY_X"] = pd.to_numeric(bin_slice["AVERAGE_VELOCITY_X"], errors='coerce').mean()
        bin["AVERAGE_VELOCITY_Y"] = pd.to_numeric(bin_slice["AVERAGE_VELOCITY_Y"], errors='coerce').mean()
        bin["RESOLUTION_X"] = pd.to_numeric(bin_slice["RESOLUTION_X"], errors='coerce').mean()
        bin["RESOLUTION_Y"] = pd.to_numeric(bin_slice["RESOLUTION_Y"], errors='coerce').mean()
        
        # add colums for area of interest fixations
        aois = bin_slice["AVERAGE_INTEREST_AREAS"]
        aoi_dict = {}
        for aoi in range(1,41):
          aoi_dict[aoi] = 0
        
        # average binned AOIs
        aoi_binned_set = set()
        
        # get AOI fixation from slice, remove 99 value  
        for aoi_list in aois:
          # parse aois to integer ids
          items = aoi_list.replace("[", "").replace("]", "").strip().split(",")
          items_aoi = []
          if len(items)>0 and items[0]:
            items_aoi = map(int or 99, items)
          if 99 in items_aoi:
            items_aoi.remove(99)
          for i in items_aoi:
            aoi_dict[i] += 1
            aoi_binned_set.add(i)
        
        aoi_sum = sum(aoi_dict.values())
        for aoi in range(1,41):
          bin["AVERAGE_IA_"+str(aoi)+"_SAMPLE_COUNT"] = aoi_dict[aoi]
          if aoi_sum == 0:
            bin["AVERAGE_IA_"+str(aoi)+"_SAMPLE_COUNT_%"] = 0
          else:
            ## the percentage should be computed with regards to overlaps, so the sum of aoi elements should not be larger than bin_size
            bin["AVERAGE_IA_"+str(aoi)+"_SAMPLE_COUNT_%"] = aoi_dict[aoi] / float(min(aoi_sum,bin_size))
            
        # add list of AOIs in the bin
        bin["AVERAGE_INTEREST_AREAS"] = str(sorted(list(aoi_binned_set)))
        #print(sorted(list(aoi_binned_set)))
        
        # append bin to the scene output
        df_scene_out = df_scene_out.append(bin)
      
        bin_start += bin_size
        bin_index += 1
      
    
   
    ## store scene 
    # create output folder
    if not os.path.exists(out_path):
      os.makedirs(out_path)
  
    # save binned data by participant and scene
    out_file = "study1_timeseries_"+str(bin_size)+"ms_bins_"
    df_scene_out.to_csv(out_path+out_file+participant+"_"+scene_name+".csv", index=False, encoding = 'utf8')
  
    
    
if __name__ == '__main__':
  start = time.time()
  
  # process files in parallel
  file_list = list(os.listdir(in_path))
  nr_processes = min(len(file_list), cpu_count())
  
  
  print("Processing {0} files using {1} parallel processes.".format(len(file_list),nr_processes))
  p = Pool(nr_processes)
  p.map(bin_file, file_list)
  
  end = time.time()
  print("Processing took {0:4.1f} minutes.".format((end - start)/60))



