from multiprocessing import Pool, cpu_count
import pandas as pd
import os, time

in_path="timeseries/4-timeseries-binned_ling_context_naacl/"
out_path="timeseries/4-timeseries-binned_ling_context_naacl/"

# read the context files
study1_filename = "timeseries/4-timeseries-binned_ling_context_naacl/study1_timeseries_20ms_bins_ling_context_par_scene_naacl.csv.gz"
study1b_filename = "timeseries/4-timeseries-binned_ling_context_naacl/study1B_timeseries_20ms_bins_ling_context_par_scene_acl.csv"

df1=pd.read_csv(study1_filename, encoding = 'utf8', compression='gzip')

df2=pd.read_csv(study1b_filename, encoding = 'utf8', header=0)


frames = [df1, df2]

dfc = pd.concat(frames)


  
  ## save files
if not os.path.exists(out_path):
	os.makedirs(out_path)
 
dfc.to_csv(out_path+"study1c_timeseries_20ms_bins_ling_context_"+"par"+"_"+"scene_acl.csv.gz", index=False, encoding = 'utf8', compression='gzip')

