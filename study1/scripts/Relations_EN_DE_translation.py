from multiprocessing import Pool, cpu_count
import pandas as pd
import os, time

in_path="orig_data/"
out_path="orig_data/"

# read the context files
context_filename = "orig_data/study1_contextual_relations.xlsx"
translation_filename = "orig_data/translation_dict.csv"


 

def get_scene_list():
  return ['A2','A3','A6','A8','A9','A10','A11','A12','A13','A14','A15','A17','A18','A19','A21','A22','A23','A24','A30']
  
scenes = get_scene_list()

# Python code to merge dict using update() method
def Merge(dict1, dict2):
    return(dict1.update(dict2))


def process_relationfile():
  """ 
  This script first splits the contextual file into AOI info and properties. Then it computetes referential complexity and similarity
  """
  df=pd.read_excel(context_filename, sheet_name = None, encoding = 'utf8')

  translatedwriter = pd.ExcelWriter(out_path + "/study1_contextual_relations_wtranslation.xlsx", engine = 'openpyxl', encoding = 'utf8')
  
  df_property = pd.DataFrame(columns = ['StudyNo', 'ImageNo','Predicate', 'Relation', 'Argument', 'Argument_DE', 'Predicate_DE']) 

  for scene in df.keys():
    if (scene == "features" or scene=="DE_EN_list" or scene=="A16"):
      continue
    
    print("Processing scene: "+scene)
    df_scene=pd.read_excel(context_filename, sheet_name = scene, encoding = 'utf8')

    # store properties
    df_property = translation(scene)
    df_property.to_excel(translatedwriter, sheet_name = scene, index=None)
 
  #df_property.to_csv(out_path + "/study1_translated.csv", sheet_name = scene, index=False, encoding = 'utf8')
    
  translatedwriter.save()
  translatedwriter.close()
  
    
    
def translation(scene):
  df_dict = pd.read_csv(translation_filename,encoding = 'utf8')

  # get overlap dictionary {"item", "containing_item"}
  df_dict.fillna("NA", inplace = True)
  general_dict = dict(zip(df_dict.Argument, df_dict['Argument_DE']))
  #print(general_dict)
  
  df_scene = pd.read_excel(context_filename,sheet_name=scene, encoding = 'utf8')
  #print(df_scene)
  scene_dict = dict(zip(df_scene.Context_label, df_scene['Context_label_DE']))
  #print(scene_dict)
    
  #overall_dict = general_dict.update(scene_dict)
  #print(overall_dict)
  
  
  # check overlapping desegmentation, adjust values
  for index, row in df_scene.iterrows():
    for key in general_dict.keys():
      # if we have an overlap between key and its containing_item (!=NA)
      if row["Argument"] ==key :
        value = general_dict[key]
        df_scene.at[index, "Argument_DE"] = value
    for key in general_dict.keys():
      # if we have an overlap between key and its containing_item (!=NA)
      if row["Predicate"] ==key :
        value = general_dict[key]
        df_scene.at[index, "Predicate_DE"] = value
  
  
  #OA: since merging two dictionaries resulted in none, we are going over the arguments two times with two different dicts; it is not efficient but works! It needs to be fixed!      
  # check overlapping desegmentation, adjust values
  for index, row in df_scene.iterrows():
    for key in scene_dict.keys():
      #print(key, scene_dict[key])
      
      # if we have an overlap between key and translation
      if row["Argument"] ==key:
        value = scene_dict[key]
        df_scene.at[index, "Argument_DE"] = value
      else:
        key= str(key)
        short_key = key.split('_')[0]
        if row["Argument"] == short_key:
          value = scene_dict[key]
          df_scene.at[index, "Argument_DE"] = value.split('_')[0]  
  for index, row in df_scene.iterrows():
    for key in scene_dict.keys():
      #print(key, scene_dict[key])
      
      # if we have an overlap between key and translation
      if row["Predicate"] ==key:
        value = scene_dict[key]
        df_scene.at[index, "Predicate_DE"] = value  
  
  return df_scene


#final=translation()
process_relationfile()

