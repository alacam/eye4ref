from multiprocessing import Pool, cpu_count
import pandas as pd
import numpy as np
import os, time
from pydoc import help

in_path="orig_data/fixationbased_reports/"
out_path="fixationbased/1-fixationbased_by_scene/"

def bin_file(file):
  """ 
  This script splits the trial for each user by scene
  """
  
  current = os.path.join(in_path, file)
  print("Processing " + file)
  
  ##### this code will work until these files are manually inspected!
  if (file.endswith("r.csv") or file.endswith("15.csv") or file.endswith("13.csv")):
    print("Skipping file " + file)
    return
  
  df=pd.read_csv(current, encoding = 'utf8', low_memory=False)

  ## remove unneccessary columns
  df.drop(inplace=True, columns=["GROUPING_VARIABLES"])
  
  
  
  # Drop the columns from fixationbased; 
  df.drop(inplace=True, columns=["IP_LABEL"])
  df.drop(inplace=True, columns=["CURRENT_FIX_MSG_TEXT_1"])
  df.drop(inplace=True, columns=["CURRENT_FIX_INTEREST_AREA_LABEL"])
  df.drop(inplace=True, columns=["CURRENT_FIX_INTEREST_AREA_PIXEL_AREA"])
  df.drop(inplace=True, columns=["CURRENT_FIX_INTEREST_AREA_X_OFFSET"])
  df.drop(inplace=True, columns=["CURRENT_FIX_INTEREST_AREA_Y_OFFSET"])
  df.drop(inplace=True, columns=["CURRENT_FIX_REFIX_INTEREST_AREA"])
  df.drop(inplace=True, columns=["CURRENT_FIX_REFIX_PREV_INTEREST_AREA"])
  df.drop(inplace=True, columns=["CURRENT_FIX_NEAREST_INTEREST_AREA"])
  df.drop(inplace=True, columns=["CURRENT_FIX_INTEREST_AREA_GROUP"])
  df.drop(inplace=True, columns=["CURRENT_FIX_INTEREST_AREA_DWELL_TIME"])
  df.drop(inplace=True, columns=["CURRENT_FIX_NEAREST_INTEREST_AREA_DISTANCE"])
  df.drop(inplace=True, columns=["CURRENT_FIX_INTEREST_AREA_RUN_ID"])
  df.drop(inplace=True, columns=["CURRENT_FIX_MSG_LIST_TEXT"])
  df.drop(inplace=True, columns=["PREVIOUS_SAC_END_INTEREST_AREAS"])
  df.drop(inplace=True, columns=["NEXT_SAC_START_INTEREST_AREAS"])
  df.drop(inplace=True, columns=["ambtype"])
  df.drop(inplace=True, columns=["TRIAL_FIXATION_TOTAL"])



  # get participant id
  participant = df.RECORDING_SESSION_LABEL.values[0]
  
  # grouping input df by scene
  scene_groups=df.groupby(df.ItemNO)

  for scene_name,scene in scene_groups:
    print("Processing scene: "+scene_name)
    # remove filler trials
    if (scene_name == "." or scene_name == "UNDEFINED" or scene_name == "UNDEFINEDnull"):
      print("... skipping filler trial")
      continue
    
    # initialize input and output scene df
    df_scene = scene_groups.get_group(scene_name)
   
    
    ## store scene 
    # create output folder
    if not os.path.exists(out_path):
      os.makedirs(out_path)
  
    # save binned data by participant and scene
    out_file = "study2_fixation_based_"
    df_scene.to_csv(out_path+out_file+participant+"_"+scene_name+".csv", index=False, encoding = 'utf8')
  
    
    
if __name__ == '__main__':
  start = time.time()
  
  # process files in parallel
  file_list = list(os.listdir(in_path))
  nr_processes = min(len(file_list), cpu_count())
  
  print("Processing {0} files using {1} parallel processes.".format(len(file_list),nr_processes))
  p = Pool(nr_processes)
  p.map(bin_file, file_list)
  
  end = time.time()
  print("Processing took {0:4.1f} minutes.".format((end - start)/60))


