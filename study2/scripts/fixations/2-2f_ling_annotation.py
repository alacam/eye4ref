from multiprocessing import Pool, cpu_count
import pandas as pd
import sys, os, time


in_path="fixationbased/1-fixationbased_by_scene/"
out_path="fixationbased/2-fixationbased_by_scene_ling/"

ling_annotations="orig_data/study1_timings_ling_annotations_amb.xlsx"
EM_messagefile="orig_data/study1_SR_message_timings_export.csv"

min_fix_duration = 80


def add_linguistic_info(start_time, fix_start, fix_end, index, df, dfl):
  """ Helper function to copy linguistic information from the linguistic description files that contain lemma form, POS-tags, dependency relations, target identification etc. """
  wi = dfl.loc[(dfl["word_onset"] <= fix_start-start_time) & (dfl["word_offset"] >= fix_start+min_fix_duration-start_time) 
  | (dfl["word_offset"] >= fix_end-start_time) & (dfl["word_onset"] <= fix_end-min_fix_duration-start_time)
  | (dfl["word_onset"] <= fix_start-start_time) & (dfl["word_offset"] >= fix_end-start_time)
  | (dfl["word_onset"] >= fix_start-start_time) & (dfl["word_offset"] <= fix_end-start_time),["word_index"]]

  # combine possible multiple words in a fixation into a list
  if len(wi) > 0:
    word_indices = []
    labels = []
    word_onsets = []
    word_offsets = []
    word_durations = []
    continuations = []
    word_des = []
    word_ens = []
    refwords = []
    task_targets = []
    object_labels = []
    object_ids = []
    lemma_des = []
    ling_indices = []
    deps = []
    pos_1s = []
    pos_2s = []
    
    for ind in wi.values:
      linfo = dfl[dfl["word_index"] == ind[0]]
      word_indices.append(int(linfo["word_index"].values[0]))
      labels.append(linfo["Label"].values[0])
      word_onsets.append(int(linfo["word_onset"].values[0]))
      word_offsets.append(int(linfo["word_offset"].values[0]))
      word_durations.append(int(linfo["word_duration"].values[0]))
      try:
        continuations.append(int(linfo["Continuation"].values[0]))
      except ValueError:
        continuations.append(0)
      word_des.append(linfo["wordDE"].values[0])
      word_ens.append(linfo["wordEN"].values[0])
      refwords.append(linfo["REFWord"].values[0])
      task_targets.append(linfo["TargetObj"].values[0])
      object_labels.append(linfo["ObjectLabel"].values[0])
      object_ids.append(linfo["ObjectID"].values[0])
      lemma_des.append(linfo["LemmaDE"].values[0])
      ling_indices.append(linfo["LingIndex"].values[0])
      deps.append(linfo["DepS"].values[0])
      pos_1s.append(linfo["pos_1"].values[0])
      pos_2s.append(linfo["pos_2"].values[0])
      
    df.at[index, "word_index"] = word_indices
    df.at[index, "Label"] = labels
    df.at[index, "word_onset"] = word_onsets
    df.at[index, "word_offset"] = word_offsets
    df.at[index, "word_duration"] = word_durations
    df.at[index, "Continuation"] = continuations
    df.at[index, "wordDE"] = word_des
    df.at[index, "wordEN"] = word_ens
    df.at[index, "REFWord"] = refwords
    df.at[index, "TargetObj"] = task_targets
    df.at[index, "ObjectLabel"] = object_labels
    df.at[index, "ObjectID"] = object_ids
    df.at[index, "LemmaDE"] = lemma_des
    df.at[index, "LingIndex"] = ling_indices
    df.at[index, "DepS"] = deps
    df.at[index, "pos_1"] = pos_1s
    df.at[index, "pos_2"] = pos_2s
  """if len(wi) > 1:
    print("multiple values")
    print(df['ItemNO'])
    print(start_time, fix_start, fix_end)
    #sys.exit(1)
"""

# input xlsx sheet with linguistic description
lingfile=pd.ExcelFile(ling_annotations, encoding = 'utf8')

# input eye momement message file
dfm=pd.read_csv(EM_messagefile, encoding = 'utf8')


def ling_annotate_file(file):
  current = os.path.join(in_path, file)
  print("Processing file " + file)

  # input
  df=pd.read_csv(current, encoding = 'utf8')
  participant = df['RECORDING_SESSION_LABEL'].values[0]
  scene = df['ItemNO'].values[0]
  #trial_start = df['TRIAL_START_TIME'].values[0]
  print("participant: {0}\t scene: {1}".format(participant, scene))
  
  # open worksheet with scene
  dfl=pd.read_excel(lingfile,scene)
  
  # get start of audio file playing
  # add 200 ms saccade delay
  audio_file = scene.replace("A", "S") + ".wav"
  start_audio_d = dfm.loc[(dfm["RECORDING_SESSION_LABEL"]==participant) & (dfm["CURRENT_MSG_TEXT"]==audio_file),["CURRENT_MSG_TIME"]]
  synctime = dfm.loc[(dfm["RECORDING_SESSION_LABEL"]==participant) & (dfm["CURRENT_MSG_TEXT"]=="SYNCTIME"),["CURRENT_MSG_TIME"]].values[0][0]
  if len(start_audio_d > 0):
    start_audio = start_audio_d.values[0][0]
    #start_audio += trial_start
    start_audio += 200
  else:
    sys.stderr.write("\nincomplete data -- skipping file...\n")
    return
  
  # get end of audio file playing, based on the start 
  end_audio = dfl.loc[dfl["Label"]=="End",["word_onset"]].values[0][0]
  #print("end audio: ", end_audio)
  end_audio += start_audio
  
  # get end of sentence
  end_sentence = dfl.loc[dfl["word_offset"].idxmax(), "word_offset"]
  #print ("end sentence: ", end_sentence)
  end_sentence += start_audio
 
  # create column for trial information (preview, taskpreview, task, post)
  df["viewtype"] = None
  
  # create columns for linguistic information
  df["word_index"] = None
  df["Label"] = None
  df["word_onset"] = None
  df["word_offset"] = None
  df["word_duration"] = None
  df["Continuation"] = None
  df["wordDE"] = None
  df["wordEN"] = None
  df["REFWord"] = None
  df["TargetObj"] = None
  df["ObjectLabel"] = None
  df["ObjectID"] = None
  df["LemmaDE"] = None
  df["LingIndex"] = None
  df["DepS"] = None
  df["pos_1"] = None
  df["pos_2"] = None
  
  # add correct audio file and image name
  df["audiofilename"] = audio_file
  df["imagefilename"] = scene+".png"
  
  # cut off data before trial synctime
  df.drop(df[df.CURRENT_FIX_START < synctime].index, inplace=True)
  # cut off data after trial end
  df.drop(df[df.CURRENT_FIX_START > end_audio].index, inplace=True)
  
  
  # for each time stamp in CURRENT_FIX_START
  # Lookup current word in linguistic description
  # add linguistic desciption to row (multiple values possible)
  
  fix_index = 1 
  for index, row in df.iterrows():
    # get CURRENT_FIX_START and END
    fix_start = row['CURRENT_FIX_START']
    fix_end = row['CURRENT_FIX_END']
    
    # get viewtype based on the time offset
    if fix_start < (start_audio - 200):
      df.at[index, "viewtype"] = "preview"
    elif fix_start >= (start_audio - 200) and fix_start < start_audio:
      df.at[index, "viewtype"] = "taskpreview"
    elif fix_start < end_sentence:
      df.at[index, "viewtype"]= "task"
      # set default Label "break" during audio playing
      df.at[index, "Label"]= "break"
    else:
      df.at[index, "viewtype"] = "post"

    # add linguistic information  
    add_linguistic_info(start_audio, fix_start, fix_end, index, df, dfl)
    
    # re-index fixations
    df.at[index, "CURRENT_FIX_INDEX"] = fix_index
    fix_index+=1
  
  # save scene--participant trial
  if not os.path.exists(out_path):
    os.makedirs(out_path)
    
  df.to_csv(out_path+"study1_fixationbased_ling_"+participant+"_"+scene+".csv", index=False, encoding = 'utf8')



if __name__ == '__main__':
  start = time.time()
  
  # process files in parallel
  file_list = list(os.listdir(in_path))
  nr_processes = min(len(file_list), cpu_count())
  print("Processing {0} files using {1} parallel processes.".format(len(file_list),nr_processes))
  
  p = Pool(nr_processes)
  p.map(ling_annotate_file, file_list)
  #for f in sorted(file_list):
  #  ling_annotate_file(f)
  #ling_annotate_file("study1_fixation_based_p10_A9.csv")
  
  end = time.time()
  print("Processing took {0:4.1f} seconds.".format((end - start)/1))
