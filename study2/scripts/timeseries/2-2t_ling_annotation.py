from multiprocessing import Pool, cpu_count
import pandas as pd
import sys, os, time

in_path="timeseries/1-timeseries-binned/"
out_path="timeseries/2-timeseries-binned_ling/"

# input xlsx sheet with linguistic description
ling_annotations="orig_data/study2_timing_ling_labels.xlsx"
lingfile=pd.ExcelFile(ling_annotations, encoding = 'utf8')


# input eye momement message file
EM_messagefile="orig_data/study2_SR_message_timings_export.csv"
dfm=pd.read_csv(EM_messagefile, encoding = 'utf8')


def add_linguistic_info(start_time, bin_time, index, df, dfl):
  """# Helper function to copy linguistic information from the linguistic description files that contain lemma form, POS-tags, dependency relations, target identification etc. """
  wi = dfl.loc[(dfl["word_onset"] <= bin_time-start_time) & (dfl["word_offset"] >=bin_time-start_time),["word_index"]]
  if len(wi) > 0:
    linfo = dfl[dfl["word_index"] == wi.values[0][0]]
    #print(linfo)
    df.at[index, "word_index"] = linfo["word_index"].values[0]
    df.at[index, "Label"] = linfo["Label"].values[0]
    df.at[index, "word_onset"] = linfo["word_onset"].values[0]
    df.at[index, "word_offset"] = linfo["word_offset"].values[0]
    df.at[index, "word_duration"] = linfo["word_duration"].values[0]
    #df.at[index, "Continuation"] = linfo["Continuation"].values[0]
    df.at[index, "wordDE"] = linfo["wordDE"].values[0]
    df.at[index, "wordEN"] = linfo["wordEN"].values[0]
    df.at[index, "REFWord"] = linfo["REFWord"].values[0]
    df.at[index, "TargetObj"] = linfo["TargetObj"].values[0]
    df.at[index, "OtherCommObj"] = linfo["OtherCommObj"].values[0]
    df.at[index, "ObjectLabel"] = linfo["ObjectLabel"].values[0]
    df.at[index, "ObjectID"] = linfo["ObjectID"].values[0]
    df.at[index, "LemmaDE"] = linfo["LemmaDE"].values[0]
    df.at[index, "LingIndex"] = linfo["LingIndex"].values[0]
    df.at[index, "DepS"] = linfo["DepS"].values[0]
    df.at[index, "pos_1"] = linfo["pos_1"].values[0]
    df.at[index, "pos_2"] = linfo["pos_2"].values[0]
  



def ling_annotate_file(file):
  current = os.path.join(in_path, file)
  print("Processing file " + file)

  # input
  df=pd.read_csv(current, encoding = 'utf8')
  participant = df['RECORDING_SESSION_LABEL'].values[0]
  scene = df['ItemNO'].values[0]
  
  toskip=["S1_1", "S1_2"]
  if scene in toskip or scene=="features":
    return
    
  trial_start = df['TRIAL_START_TIME'].values[0]
  print("participant: {0}\t scene: {1}".format(participant, scene))
  
  # open worksheet with scene
  dfl=pd.read_excel(lingfile,scene)
  
  # get start of audio file playing
  # add 200 ms saccade delay
  audio_file = scene.replace("A", "S") + ".wav"
  start_audio_d = dfm.loc[(dfm["RECORDING_SESSION_LABEL"]==participant) & (dfm["CURRENT_MSG_TEXT"]==audio_file),["CURRENT_MSG_TIME"]]
  synctime = dfm.loc[(dfm["RECORDING_SESSION_LABEL"]==participant) & (dfm["CURRENT_MSG_TEXT"]=="SYNCTIME"),["CURRENT_MSG_TIME"]].values[0][0]
  if len(start_audio_d > 0):
    start_audio = start_audio_d.values[0][0]
    start_audio += trial_start
    start_audio += 200
  else:
    sys.stderr.write("\nincomplete data -- skipping file" + file+"\n")
    return
  
  # get end of audio file playing, based on the start 
  end_audio = dfl.loc[dfl["Label"]=="End",["word_onset"]].values[0][0]
  #print("end audio: ", end_audio)
  end_audio += start_audio
  
  # get end of sentence
  end_sentence = dfl.loc[dfl["word_offset"].idxmax(), "word_offset"]
  #print ("end sentence: ", end_sentence)
  end_sentence += start_audio
 
  # create column for trial information (preview, taskpreview, task, post)
  df["viewtype"] = None
  
  # create columns for linguistic information
  df["word_index"] = None
  df["Label"] = None
  df["word_onset"] = None
  df["word_offset"] = None
  df["word_duration"] = None
  #df["Continuation"] = None
  df["wordDE"] = None
  df["wordEN"] = None
  df["REFWord"] = None
  
  df["TargetObj"] = None
  df["OtherCommObj"] = None
  
  df["ObjectLabel"] = None
  df["ObjectID"] = None
  df["LemmaDE"] = None
  df["LingIndex"] = None
  df["DepS"] = None
  df["pos_1"] = None
  df["pos_2"] = None
  
  # add correct audio file and image name
  df["audiofilename"] = audio_file
  df["imagefilename"] = scene+".png"
  
  # cut off data before trial synctime
  df.drop(df[df.BIN_START_TIME < synctime+trial_start].index, inplace=True)
  # cut off data after trial end
  df.drop(df[df.BIN_START_TIME > end_audio].index, inplace=True)
  
  
  # for each time stamp in BIN_START_TIME
  # Lookup current word in linguistic description
  # add linguistic desciption to row
  
  bin_index = 1 
  for index, row in df.iterrows():
    # get BIN_START_TIME
    bin_time = row['BIN_START_TIME']
    
    # get viewtype based on the time offset
    if bin_time < (start_audio - 200):
      df.at[index, "viewtype"] = "preview"
    elif bin_time >= (start_audio - 200) and bin_time < start_audio:
      df.at[index, "viewtype"] = "taskpreview"
    elif bin_time < end_sentence:
      df.at[index, "viewtype"]= "task"
      # set default Label "break" during audio playing
      df.at[index, "Label"]= "break"
    else:
      df.at[index, "viewtype"] = "post"

    # add linguistic information  
    add_linguistic_info(start_audio, bin_time, index, df, dfl)
    
    # re-index bins
    df.at[index, "BIN_INDEX"] = bin_index
    bin_index+=1
  
  # save scene--participant trial
  if not os.path.exists(out_path):
    os.makedirs(out_path)
    
  df.to_csv(out_path+"study2_timeseries_20ms_bins_ling_"+participant+"_"+scene+".csv", index=False, encoding = 'utf8')



if __name__ == '__main__':
  start = time.time()
  
  # process files in parallel
  file_list = list(os.listdir(in_path))
  nr_processes = min(len(file_list), cpu_count())
  print("Processing {0} files using {1} parallel processes.".format(len(file_list),nr_processes))
  
  p = Pool(nr_processes)
  p.map(ling_annotate_file, file_list)
  
  end = time.time()
  print("Processing took {0:4.1f} seconds.".format((end - start)/1))
